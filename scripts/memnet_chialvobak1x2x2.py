"""
    Simple Chialvo-Bak network
"""
# Copyright (C) 2020 Juan Pablo Carbajal
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

# Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>

import numpy as np
import matplotlib.pyplot as plt

import memnet.components as cp
import memnet.learningmachines as lm

# Network definition
GND = 0
inputs = [
    (GND, 1, {'component': cp.VoltageSource(), 'input': True})
]
outputs = [
    (2, GND, {'output': True}),
    (3, GND, {'output': True})
]
interface_in = [
    (1, 4, {'component': cp.Memristor(memristance=np.random.rand())}),
    (1, 5, {'component': cp.Memristor(memristance=np.random.rand())})
]
interface_out = [
    (4, 2, {'component': cp.Memristor(memristance=np.random.rand())}),
    (4, 3, {'component': cp.Memristor(memristance=np.random.rand())}),
    (5, 3, {'component': cp.Memristor(memristance=np.random.rand())})
]

task_table = np.array([
    [0, 1],
])

def main():

    # Create learning machine
    machine = lm.ChialvoBak()
    for e in inputs + outputs + interface_in + interface_out:
        machine.add_component(e[0], e[1], **e[2])
    machine.freeze_topology()

    res = machine.learn(task_table, corrections=20, return_ts=True)
    print(f'\tError: {res.error[-1]}'
          + f'\tCorrections: {res.corrections}')

    return machine, res

if __name__ == '__main__':
    m, res = main()