"""
    Reproduction of the results presented in::

        Chialvo, D.R., and BAK, P. (1999). Learning from mistakes.
        Neuroscience Vol. 90, No. 4, pp. 1137–1148
"""
# Copyright (C) 2020 Juan Pablo Carbajal
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

# Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>

import numpy as np
import matplotlib.pyplot as plt

from ttictoc import tic, toc

from memnet.utils import function_to_io
import memnet.learningmachines as lm

#: List of mappings as functions from [0,1] to itself
TASKS = {
    'a': lambda x: x,
    'b': lambda x: 1.0 - x,
    'c': lambda x: np.where(x < 0.5, x, 1.0 - x),
    'd': lambda x: np.where(x < 0.5, 1.0 - x, x),
    'e': lambda x: np.mod(2 * x, 1.0 + 1e-8),
    'f': lambda x: np.mod(x, 0.5 + 1e-8),
}

iosize = dict(n_in=10, n_out=10)
rndgen = np.random.default_rng()


def create_machine():
    # Create learning machine
    machine = lm.ChialvoBak(teach_amplitude=-0.005 * iosize['n_in'])
    machine.topology_feedforward(n_middle=0, **iosize)
    machine.freeze_topology()
    # memristance randomizer
    mem = machine.memristance_array(reduced=True)[0]
    np.random.seed(123454321)
    machine.set_memristance(rndgen.uniform(0.2, 0.6, mem.shape))
    return machine


def main():

    machine = create_machine()
    mem = machine.memristance_array(reduced=True)[0]
    print(machine)

    # Iterate over tasks and try to learn
    tsk_tables = []
    errorts = []
    tskts = []
    iots = []
    ioend = []
    for label, taskfun in TASKS.items():
        tic()
        tsk_tables.append(function_to_io(taskfun, **iosize))

        # Prime the network
        # Assume we can more or less randomize it
        #machine.set_memristance(rndgen.uniform(0.2, 0.6, mem.shape))

        # has alsmot the same effect
        #orders = rndgen.integers(iosize['n_in'],
        #                                          size=(3, iosize['n_in']))
        #for order in orders:
        #    machine.process_inputs_from(tsk_tables[-1][order, 0], reinforce=0.5)

        #res = machine.learn(tsk_tables[-1], corrections=20 * iosize['n_in'])
        res = machine.learn(tsk_tables[-1], iterations=20)
        print(f'Task {label}:\n\tError: {res.error[-1]}'
              + f'\tIterations: {res.iterations}'
              + f'\tCorrections: {res.corrections}')
        print(f'Elapsed time {toc()} s')
        # check output
        final_io = machine.process_inputs_from(tsk_tables[-1][:, 0])

        # Book keeping
        errorts.append(res.error)
        tskts = tskts + [label] * res.error.size
        iots.append(res.io)
        ioend.append(final_io)

    # Convert lists to numpy arrays for easy plotting
    ntsk = len(TASKS)
    errcode = np.array([e[-1] for e in errorts]).dot(iosize['n_in']**np.arange(ntsk))
    print(f'Error code: {errcode}')
    errorts = np.concatenate(errorts)
    tskts = np.array(tskts)
    time = np.arange(1, errorts.size + 1)

    # Plot
    fig = plt.figure(constrained_layout=True)
    gs = fig.add_gridspec(ntsk, 4)
    nsubs = int(np.floor(ntsk / 2))
    ax_errorts = fig.add_subplot(gs[:nsubs, :3])
    ax_tskts = fig.add_subplot(gs[nsubs:, :3], sharex=ax_errorts)
    ax_tasks = []
    for n in range(ntsk):
        if ax_tasks:
            ax_tasks.append(fig.add_subplot(gs[n, 3],
                                            sharex=ax_tasks[-1],
                                            sharey=ax_tasks[-1]))
        else:
            ax_tasks.append(fig.add_subplot(gs[n, 3]))

    tcolor = np.unique(tskts, return_inverse=True)[1]
    s_fmt = dict(s=16, c=tcolor, cmap='Set1')
    l_fmt = dict(linestyle='-', color='k', linewidth=1, alpha=0.5, zorder=0.0)
    ax_errorts.scatter(time, errorts, **s_fmt)
    ax_errorts.plot(time, errorts, **l_fmt)
    ax_errorts.set_ylabel('Error')
    ax_errorts.set_ylim((-0.5, iosize['n_out']+0.5))

    ax_tskts.plot(time, tskts, **l_fmt)
    ax_tskts.scatter(time, tskts, **s_fmt)
    ax_tskts.set_xlabel('Iterations')
    ax_tskts.set_ylabel('Task')

    first = True
    for ax, tsk, io, lbl in zip(ax_tasks, tsk_tables, ioend, TASKS.keys()):
        ax.plot(tsk[:, 0], tsk[:, 1], '.:')
        ax.plot(io[:, 0], io[:, 1], '.')
        if first:
            ax.set_ylim(-0.5, iosize['n_out'] - 1 + 0.5)
            ax.set_xlim(-0.5, iosize['n_in'] - 1 + 0.5)
            first = False
        ax.set_xticks([])
        ax.set_yticks([])
        ax.set_ylabel(lbl)

    plt.show()

    return machine, iots, ioend, tsk_tables, errcode


if __name__ == '__main__':
    mch, res, io, tt, err = main()
