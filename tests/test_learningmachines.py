# Copyright (C) 2020 Juan Pablo Carbajal
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

# Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>

import unittest

import numpy as np

from memnet import learningmachines as lm

class TestChialvoBakMethods(unittest.TestCase):
    def setUp(self):
        self.da = 0.1
        self.ta = -10
        self.ds = 10
        self.m = lm.ChialvoBak(teach_amplitude=self.ta, data_amplitude=self.da, samples=self.ds)

    def tearDown(self):
        self.m = lm.ChialvoBak()

    def test_data_amplitude(self):
        self.assertEqual(self.m.data_amplitude, self.da)
        self.m.data_amplitude = 0.2
        self.assertEqual(self.m.data_amplitude, 0.2)

    def test_teach_amplitude(self):
        self.assertEqual(self.m.teach_amplitude, self.ta)
        self.m.teach_amplitude = -20
        self.assertEqual(self.m.teach_amplitude, -20)

    def test_data_steps(self):
        self.assertEqual(self.m.samples, self.ds)
        self.m.samples = 20
        self.assertEqual(self.m.samples, 20)
        self.assertEqual(self.m._t.size, 20)
        self.assertEqual(self.m.time_step, 1/19)

    def test_time_step(self):
        self.m.time_step = 0.01
        self.assertEqual(self.m.time_step, 0.01)
        self.assertEqual(self.m.samples, 101)

if __name__ == '__main__':
    unittest.main()
