# Copyright (C) 2020 Juan Pablo Carbajal
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

# Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>

import unittest

import networkx as nx

from memnet import cycle_space as cs


class TestCycleSpace(unittest.TestCase):

    def setUp(self):
        self.cables = [(1, 2), (1, 2), (1, 2), (3, 1), (3, 2)]

    def test_undirected(self):
        # Testing undirected graph
        Gu = nx.Graph(self.cables)
        Cu, Tu = cs.chords(Gu)
        Mu = cs.cycle_space_matrix(Gu, Cu, Tu)
        Zu = cs.cycle_space(Cu, Tu)

    def test_undirected_multi(self):
        # Testing undirected multigraph
        Gm = nx.MultiGraph(self.cables)
        Cm, Tm = cs.chords(Gm)
        Mm = cs.cycle_space_matrix(Gm, Cm, Tm)
        Zm = cs.cycle_space(Cm, Tm)

    def test_directed(self):
        # Testing directed graph
        Gd = nx.DiGraph(self.cables)
        Cd, Td = cs.chords(Gd)
        Md = cs.cycle_space_matrix(Gd, Cd, Td)
        Zd = cs.cycle_space(Cd, Td)

    def test_directed_multi(self):
        # Testing directed multigraph
        Gmd = nx.MultiDiGraph(self.cables)
        Cmd, Tmd = cs.chords(Gmd)
        Mmd = cs.cycle_space_matrix(Gmd, Cmd, Tmd)
        Zmd = cs.cycle_space(Cmd, Tmd)
