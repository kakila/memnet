import unittest

from memnet import components as cp


class TestCompoenents(unittest.TestCase):

    def test_resistor(self):
        r = cp.Resistor(110)
        self.assertEqual(r.resistance, 110)
        keys = ['current', 'voltage', 'resistance', 'Resistor']
        self.assertEqual(keys, list(r.asdict().keys()))

    def test_memristors(self):
        m = cp.Memristor(memristance=0.5, low_R=10, high_R=20,
                         polarity=-1, gain=10)
        self.assertEqual(m.memristance, 0.5)
        self.assertEqual(m.low_R, 10)
        self.assertEqual(m.high_R, 20)
        self.assertEqual(m.polarity, -1)
        self.assertEqual(m.gain, 10)
        self.assertEqual(m.resistance, 15)

    def test_memristorsparameters(self):
        self.assertEqual(set(cp.MemristorParameters._fields),
                         {'low_R', 'high_R', 'gain', 'polarity', 'series_R'})


if __name__ == '__main__':
    unittest.main()
