
import unittest

import sympy as sp
import numpy as np

from memnet import electricnetwork as en
from memnet import components as cp
from memnet.electricnetwork import solve_currentbased


class TestElectricNetworkSymbolicSolvers(unittest.TestCase):
    def setUp(self):
        self.Vcc = sp.symbols(r'V', real=True)
        self.resistors = sp.symbols(r'R1:4', real=True, positive=True)
        self.n = en.ElectricNetwork()


    def test_solve_currentbased_parallel(self):
        self.n.add_component(1, 2, component=cp.Resistor(self.resistors[0]))
        self.n.add_component(1, 2, component=cp.Resistor(self.resistors[1]))
        self.n.add_component(1, 2, component=cp.VoltageSource(self.Vcc))

        I, V, extr = solve_currentbased(resistance=self.n.resistance_array, source=self.n.voltage_array,
                                        KVL=self.n.kvl, KCL=self.n.kcl)
        Req_inv = (self.resistors[0] + self.resistors[1]) / (self.resistors[0] * self.resistors[1])
        self.assertEqual(I[self.n.voltage_source_index], self.Vcc * Req_inv)

    def test_solve_currentbased_single(self):
        self.n.add_component(1, 2, component=cp.VoltageSource(self.Vcc))
        self.n.add_component(1, 2, component=cp.Resistor(self.resistors[0]))

        I, V, extr = solve_currentbased(resistance=self.n.resistance_array, source=self.n.voltage_array,
                                        KVL=self.n.kvl, KCL=self.n.kcl)
        self.assertEqual(I[self.n.voltage_source_index], self.Vcc / self.resistors[0])

    def test_solve_currentbased_parallelseries(self):
        self.n.add_component(1, 2, component=cp.VoltageSource(self.Vcc))
        self.n.add_component(2, 3, component=cp.Resistor(self.resistors[0]))
        self.n.add_component(2, 3, component=cp.Resistor(self.resistors[1]))
        self.n.add_component(3, 1, component=cp.Resistor(self.resistors[2]))

        I, V, extr = solve_currentbased(resistance=self.n.resistance_array, source=self.n.voltage_array,
                                        KVL=self.n.kvl, KCL=self.n.kcl)
        Req = (self.resistors[0] * self.resistors[1]) / (self.resistors[0] + self.resistors[1]) + self.resistors[2]
        I_ = (self.Vcc / Req).together().expand()
        self.assertEqual(I[self.n.voltage_source_index][0].expand(), I_)

if __name__ == '__main__':
    unittest.main()
