# Copyright (C) 2020 Juan Pablo Carbajal
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

# Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>

import unittest

import numpy as np

from memnet import memristornetwork as mn
from memnet import components as cp


class TestMemrisotrNetworkMethods(unittest.TestCase):
    def setUp(self):
        self.net = mn.MemristorNetwork()
        self.net.add_component(0, 1, component=cp.Memristor(memristance=1))
        self.net.add_component(1, 2, component=cp.Memristor(memristance=1/2))
        self.net.add_component(1, 2, component=cp.Resistor())
        self.net.add_component(2, 0, component=cp.Resistor())
        self.net.add_component(2, 0, component=cp.Resistor())

    def tearDown(self):
        self.net = mn.MemristorNetwork()

    def test_memristance_array(self):
        mems, idx = self.net.memristance_array(reduced=True)
        self.assertEqual(mems.tolist(), [1.0, 1/2])
        self.assertEqual(idx.tolist(), [0, 1])
        mems = self.net.memristance_array()
        mems[np.isnan(mems)] = -1
        self.assertEqual(mems.tolist(), [1.0, 1/2] + [-1] * (mems.size-2))

    def test_memristance_array_index(self):
        mems, idx = self.net.memristance_array(reduced=True)
        mems_ = self.net.memristance_array()
        self.assertEqual(mems.tolist(), mems_[idx].tolist())

    def test_set_memristance(self):
        mems, idx = self.net.memristance_array(reduced=True)
        mems[idx] = 0.5
        self.net.set_memristance(mems)
        mems = self.net.memristance_array()
        mems[np.isnan(mems)] = -1
        mlist = [0.5 if i in idx else -1 for i in range(mems.size)]
        self.assertEqual(mems.tolist(), mlist)

    def test_memristor_param_array(self):
        paramarray = self.net.memristor_param_array
        self.assertEqual(set(paramarray._fields),
                         set(cp.MemristorParameters._fields))
        self.assertEqual(set(paramarray._fields),
                         {'low_R', 'high_R', 'gain', 'polarity', 'series_R'})

    def test_integrate_network_cteV(self):
        self.net = mn.MemristorNetwork()
        self.net.add_component(0, 1, component=cp.VoltageSource(1.0))
        self.net.add_component(1, 2, component=cp.Memristor(memristance=0.0,
                                                            low_R=1.0,
                                                            high_R=3.0))
        self.net.add_component(2, 0, component=cp.Resistor(3.0))
        Vcc = np.ones((10, 1))

        ts, id_mem = self.net.integrate_network(Vcc, 0.01)
        self.assertEqual(ts.current.shape, (10, 3))
        self.assertEqual(ts.voltage.shape, (10, 3))
        self.assertEqual(ts.resistance.shape, (10, 3))
        self.assertEqual(ts.memristance.shape, (10, 1))
        self.assertTrue(
            np.allclose(ts.resistance[-1,:], [0.0, 1.0, 3.0])
        )
        I0 = Vcc[:, 0]/ts.resistance.sum(axis=1)
        self.assertTrue(np.allclose(ts.current[:, 0], I0))
        self.assertTrue(np.allclose(ts.voltage[:, 0], -1))
        self.assertTrue(np.allclose(ts.voltage[:, 1:],
                                    np.atleast_2d(I0).T * ts.resistance[:, 1:]))

if __name__ == '__main__':
    unittest.main()
