# Copyright (C) 2020 Juan Pablo Carbajal
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

# Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>

import unittest

import numpy as np

from memnet import electricnetwork as en
from memnet import components as cp


class TestElectricNetworkMethods(unittest.TestCase):
    def setUp(self):
        self.net = en.ElectricNetwork()

    def tearDown(self):
        self.net = en.ElectricNetwork()

    def test_add_component(self):
        self.net.add_component(0, 1)
        self.net.add_component(0, 1)
        self.net.add_component(0, 2)
        self.assertEqual(self.net.number_of_edges(), 3)
        for e in self.net.edges(data='Component', keys=True, default=False):
            self.assertTrue(e[3])

        self.net.add_component(1, 2, component=cp.Resistor(110))
        self.assertTrue(self.net[1][2][0]['Resistor'])

    def test_attr_array(self):
        self.net.add_component(0, 1)
        self.net.add_component(0, 1)
        self.net.add_component(0, 2)
        va = self.net.voltage_array
        va_ = self.net.attr_array('voltage')
        for v, v_ in zip(va, va_):
            self.assertEqual(v, v_)
        self.assertEqual(self.net.number_of_edges(), va.size)
        self.assertEqual(va_.size, va.size)
        self.assertEqual((va == 0).sum(), va.size)

        ia = self.net.current_array
        ia_ = self.net.attr_array('current')
        for i, i_ in zip(ia, ia_):
            self.assertEqual(i, i_)
        self.assertEqual(self.net.number_of_edges(), ia.size)
        self.assertEqual(ia_.size, ia.size)
        self.assertEqual((ia == 0).sum(), ia.size)

    def test_attr_mask(self):
        self.net.add_component(0, 1)
        self.net.add_component(0, 1)
        self.net.add_component(0, 2)
        msk = self.net.attr_mask('voltage')
        va = self.net.voltage_array

        self.assertEqual(self.net.number_of_edges(), msk.sum())

    def test_update_iv(self):
        self.net.add_component(0, 1)
        self.net.add_component(0, 1)
        self.net.add_component(1, 0)

        curr = [1,2,3]
        volt = [-3,-2,-1]
        self.net.update_iv(current=curr, voltage=volt)
        curr_, volt_ = self.net.iv_array
        self.assertEqual(curr, curr_.tolist())
        self.assertEqual(volt, volt_.tolist())

    def test_solve_iv_vdivider(self):
        self.net.add_component(0, 1, component=cp.VoltageSource(1.0))
        self.net.add_component(1, 2, component=cp.Resistor(1.0))
        self.net.add_component(2, 0, component=cp.Resistor(3.0))

        I, V, extra = self.net.solve_iv()
        self.assertTrue(np.allclose(I, [0.25, 0.25, 0.25]))
        self.assertTrue(np.allclose(V.tolist(), [-1.0, 0.25, 3*0.25]))

    def test_solve_iv_idivider(self):
        self.net.add_component(0, 1, component=cp.VoltageSource(1.0))
        self.net.add_component(1, 0, component=cp.Resistor(1.5))
        self.net.add_component(1, 0, component=cp.Resistor(3.0))

        I, V, extra = self.net.solve_iv()
        self.assertTrue(np.allclose(I, [1.0, 1/1.5, 1/3.0]))
        self.assertTrue(np.allclose(V.tolist(), [-1.0, 1.0, 1.0]))

if __name__ == '__main__':
    unittest.main()
