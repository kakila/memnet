"""
  Electric network topology generators

"""
# Copyright (C) 2020 Juan Pablo Carbajal
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

# Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>
import re

from itertools import (
    product,
    chain
)

from collections import Iterable
from functools import partial

import yaml

def tripartite(n_in, n_middle, n_out):
    """
    Edges of a complete n_in x n_middle x n_out topology

    Parameters
    ----------
    n_in,n_out: interger
        Number of inputs and outputs nodes.
        It should be a positive integer.
    n_middle: integer
        Number of nodes betwen the input and the outputs.
        It should be a non-negative integer.
        If it is 0, the inputs are directly connected to the outputs.

    Returns
    -------
    :obj:`itertools.chain`
        Iterable with the edges of the topology.

    Raises
    -----
    ValueError
        If n_in or n_out are not positive, or if n_middle is negative.

    Examples
    --------
    >>> edges = tripartite(1, 2, 1)
    >>> list(edges)
    [(0, 1), (1, 3), (1, 4), (3, 2), (4, 2), (2, 0)]

    >>> edges = tripartite(2, 0, 2)
    >>> list(edges)
    [(0, 1), (0, 2), (1, 3), (1, 4), (2, 3), (2, 4), (3, 0), (4, 0)]
    """
    if n_in < 1 or n_out < 1:
        ValueError('Number of inputs and outputs nodes should be positive.')

    nodes_in = range(1, n_in + 1)
    nodes_out = range(nodes_in[-1] + 1, nodes_in[-1] + n_out + 1)

    if n_middle == 0:
        return chain(product([0], nodes_in),
                     product(nodes_in, nodes_out),
                     product(nodes_out, [0]))

    elif n_middle > 0:
        nodes_middle = range(nodes_out[-1] + 1, nodes_out[-1] + n_middle + 1)
    else:
        raise ValueError('Number of middle nodes should be non-negative.')

    return chain(product([0], nodes_in),
                 product(nodes_in, nodes_middle),
                 product(nodes_middle, nodes_out),
                 product(nodes_out, [0]))


def npartite(n_in, n_middle, n_out):
    if n_in < 1 or n_out < 1:
        ValueError('Number of inputs and outputs nodes should be positive.')

    if not isinstance(n_middle, Iterable):
        return tripartite(n_in, n_middle, n_out)

    nodes_in = range(1, n_in + 1)
    nodes_out = range(nodes_in[-1] + 1, nodes_in[-1] + n_out + 1)

    last_node = nodes_out[-1]
    prev_nodes = nodes_in
    product_list = []
    for nm in n_middle:
        if nm == 0:
            pass
        elif nm > 0:
            nodes_middle = range(last_node + 1, last_node + nm + 1)
            product_list.append(product(prev_nodes, nodes_middle))
            prev_nodes = nodes_middle[:]
            last_node = prev_nodes[-1]
        else:
            raise ValueError('Number of middle nodes should be non-negative.')

    return chain(product([0], nodes_in),
                 *product_list,
                 product(prev_nodes, nodes_out),
                 product(nodes_out, [0]))


# YAML representation of topologies
class FeedForward(yaml.YAMLObject):
    yaml_tag = '!topology_feedforward'
    yaml_pattern = re.compile(r'^\d+[x\d]+$')

    @classmethod
    def from_str(cls, string):
        # validate string
        if not string[0].isdigit():
            raise ValueError('String should start with a digit.')
        if not string[-1].isdigit():
            raise ValueError('String should end with a digit.')
        values = tuple(map(int, string.split('x')))
        return FeedForward(values[0], values[1:-1], values[-1])

    def __init__(self, inputs, bulk, outputs):
        self.inputs = inputs
        self.outputs = outputs
        self.bulk = list(bulk) if isinstance(bulk, Iterable) else [bulk]

    def __str__(self):
        return 'x'.join(map(str, [self.inputs] + self.bulk + [self.outputs]))

    def __repr__(self):
        txt = f'{self.__class__.__name__}(' \
              + f'inputs={self.inputs}, ' \
              + f'bulk={self.bulk}, ' \
              + f'outputs={self.outputs}' \
              + ')'
        return txt

    def __call__(self):
        return npartite(self.inputs, self.bulk, self.outputs)


def general_representer(dumper, obj):
    return dumper.represent_scalar(obj.yaml_tag, str(obj))

def general_constructor(loader, node, klass):
    string = loader.construct_scalar(node)
    return klass.from_str(string)


yaml.add_representer(FeedForward, general_representer)
yaml.add_constructor(FeedForward.yaml_tag, partial(general_constructor,
                                                   klass=FeedForward))
yaml.add_implicit_resolver(FeedForward.yaml_tag, FeedForward.yaml_pattern)