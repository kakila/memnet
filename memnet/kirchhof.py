# Copyright (C) 2020 Juan Pablo Carbajal
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

# Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>

import networkx as nx
import numpy as np

from .cycle_space import (
    chords, 
    cycle_space_matrix,
    rref
)


def kvl(graph):
    """ Kirchhof voltage law for the network.
    """
    V = cycle_space_matrix(graph, *chords(graph)).T
    if np.linalg.matrix_rank(V) < V.shape[0]:
        import pdb; pdb.set_trace()
        # Delete zero rows
        tmp = rref(V.copy())
        V = np.delete(V, np.where((tmp == 0).all(axis=1)), axis=0)
    return V.astype(int)


def kcl(graph):
    """ Kirchhof current law for the network.
    """
    return nx.incidence_matrix(graph, oriented=True).toarray()[:-1].astype(int)
