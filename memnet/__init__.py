#!/usr/bin/env python3

""" Index file for module memnet"""

__author__ = "Juan Pablo Carbajal"
__copyright__ = "Copyright (C) 2020 JuanPi Carbajal"
__credits__ = "Juan Pablo Carbajal"
__license__ = """This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>."""
__version__ = "0.1.0"
__author__ = "Juan Pablo Carbajal"
__maintainer__ = ["Juan Pablo Carbajal"]
__email__ = "ajuanpi+dev@gmail.com"
__status__ = "Development"
