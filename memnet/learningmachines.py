"""
  Learning machines utilities

"""
# Copyright (C) 2020 Juan Pablo Carbajal
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

# Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>

import warnings

from inspect import signature
from collections import namedtuple
from textwrap import dedent

import numpy as np
import yaml

from .memristornetwork import MemristorNetwork
from .components import (
    VoltageSource,
    Memristor
)
from .topology import npartite


# monkeypatch showwarning
def _showwarning(message, category, filename, lineno, file=None, line=''):
    """Custom write a warning to a file"""
    msg = f'{category.__name__}: {filename}:{lineno}: {message}'
    if file is None:
        print(msg)
    else:
        print(msg, file=file)


warnings.showwarning = _showwarning

#: Storage for learning results
LearningResults = namedtuple('LearningResults', ['error', 'corrections',
                                                 'iterations', 'io', 'extra'])


class ChialvoBak(MemristorNetwork):
    """
        API for a Chialvo-Bak learning machine
    """

    @classmethod
    def from_yaml(cls, filename):
        """ Construct from a yaml file """
        with open(filename, 'r') as file:
            args = yaml.load(file, Loader=yaml.Loader)

        # extract arguments for constructor
        init_args = {k_: v for k, v in args.items()
                     if (k_ := '_'.join(k.split()))
                     in signature(cls.__init__).parameters}
        machine = ChialvoBak(**init_args)
        # Remove constructor arguments from parsed arguments
        for k in init_args.keys():
            del args[' '.join(k.split('_'))]

        # parse topology
        if 'topology' in args:
            topo = args['topology']
            # WARNING topo should be callable
            machine.topology_from_edges(topo())
            del args['topology']

        # initialize memristors
        if 'initialization' in args:
            init = args['initialization']
            if 'seed' in init:
                np.random.seed(init['seed'])
            wmin = init['min'] if 'min' in init else 0
            wmax = init['max'] if 'max' in init else 1
            # sample uniform in [wmin, wmax]
            w = wmin + (wmax - wmin) * np.random.rand(len(machine.memristor_edges))
            machine.set_memristance(w)
            del args['initialization']

        if args:
            warnings.warn('Ignored entries in YAML file: '
                          + f'{list(args.keys())}', UserWarning)
        return machine

    def __init__(self, *, teach_amplitude=-0.1, data_amplitude=0.01,
                 samples=100):
        super().__init__()

        samples = int(samples)
        if samples < 2:
            raise ValueError('At least 2 data steps are needed.')
        self._samples = int(samples)
        self._t = np.linspace(0, 1, int(self._samples))
        self._dt = self._t[1] - self._t[0]

        self._data_signal = data_signal
        self._data_amplitude = data_amplitude
        self.sample_datasignal()

        self._teach_signal = teaching_signal
        self._teach_amplitude = teach_amplitude
        self.sample_teachsignal()

        self.open_mem = dict(series_R=1e16)

        # These are filled when the topology is frozen
        self.edges_array = []
        self.input_edge_idx = []
        self.output_edge_idx = []
        self.input_out_edges_idx = []
        self.output_in_edges_idx = []

        self.memristor_edges_idx = []
        self.memristors_params = []

        self._topology_frozen = False

    def freeze_topology(self):
        self.edges_array = np.array(self.edges)
        self.input_edge_idx = np.nonzero(self.attr_array('input'))[0]
        self.output_edge_idx = np.nonzero(self.attr_array('output'))[0]
        if (self.input_edge_idx.size == 0) or (self.output_edge_idx.size == 0):
            raise ValueError('Inputs/Outputs undefined')

        msk = [False] * self.edges_array.shape[0]
        for ie in self.edges_array[self.input_edge_idx]:
            msk |= self.edges_array[:, 0] == ie[1]
        self.input_out_edges_idx = np.nonzero(msk)[0]

        msk = [False] * self.edges_array.shape[0]
        for oe in self.edges_array[self.output_edge_idx]:
            msk |= self.edges_array[:, 1] == oe[0]
        self.output_in_edges_idx = np.nonzero(msk)[0]

        _, self.memristor_edges_idx = self.memristance_array(reduced=True)
        self.memristors_params = self.memristor_param_array

        self._kvl = self.kvl
        self._kcl = self.kcl

        self._topology_frozen = True

    def __str__(self):
        """ String representation """
        n_in = np.sum(self.attr_mask('input'))
        n_out = np.sum(self.attr_mask('output'))
        n_bulk = self.number_of_edges() - n_in - n_out
        nn_bulk = self.number_of_nodes() - n_in - n_out - 1
        txt = dedent(f'''        ({self.__class__.__name__}): 
            Edges: {n_in} x {n_bulk} x {n_out}
            Nodes: {n_in} x {nn_bulk} x {n_out}
            Frozen: {self._topology_frozen}
            Data mode: 
               {func2str(self._data_signal)}
               amplitude: {self.data_amplitude}
               samples: {self._samples}
            Teach mode: 
               {func2str(self._teach_signal)}
               amplitude: {self._teach_amplitude}
               samples: {self._samples}
            ''')
        return txt

    @property
    def data_amplitude(self):
        return self._data_amplitude

    @data_amplitude.setter
    def data_amplitude(self, value):
        self._data_amplitude = value
        self.sample_datasignal()

    @property
    def teach_amplitude(self):
        return self._teach_amplitude

    @teach_amplitude.setter
    def teach_amplitude(self, value):
        self._teach_amplitude = value
        self.sample_teachsignal()

    @property
    def samples(self):
        return self._samples

    @samples.setter
    def samples(self, value):
        value = int(value)
        if value < 2:
            raise ValueError('At least 2 data steps are needed.')
        self._samples = value
        self._t = np.linspace(0, 1, self._samples)
        self._dt = self._t[1] - self._t[0]
        self.sample_datasignal()
        self.sample_teachsignal()

    @property
    def time_step(self):
        return self._dt

    @time_step.setter
    def time_step(self, value):
        self._dt = value
        self._t = np.arange(0, 1 + self._dt, self._dt)
        self.samples = self._t.size

    @property
    def data_signal(self):
        return self._insignal

    @property
    def teach_signal(self):
        return self._tchsignal

    def sample_datasignal(self):
        """ Sample the signal used for data encoding

            The result is returned and internally stored in the instance.
            It can be accessed with :attr:`data_signal`

            Returns
            -------
            :class:`numpy.ndarray`
                The sampled signal
        """
        self._insignal = np.zeros((self._samples, 1))
        self._insignal[:, 0] = self._data_signal(self._t,
                                                 amp=self._data_amplitude)
        self._insignal = self._insignal - self._insignal.mean()
        return self._insignal

    def sample_teachsignal(self):
        """ Sample the signal used for teaching

            The result is returned and internally stored in the instance.
            It can be accessed with :attr:`teach_signal`

            Returns
            -------
            :class:`numpy.ndarray`
                The sampled signal
        """
        self._tchsignal = np.zeros((self._samples, 1))
        self._tchsignal[:, 0] = self._teach_signal(self._t,
                                                   amp=self._teach_amplitude)
        return self._tchsignal

    def topology_from_edges(self, edges):
        """ Add edges to the topology of the network

            Parameters
            ----------

            Returns
            -------

            Raises
            ------
        """
        if not self._topology_frozen:
            edges = list(edges)
            gnd = min(e[0] for e in edges)
            for e in edges:
                propdict = dict()
                if e[0] == gnd:  # input
                    propdict['input'] = True
                    propdict['component'] = VoltageSource()
                elif e[1] == gnd:  # output
                    propdict['output'] = True
                else: # bulk
                    propdict['component'] = Memristor()
                self.add_component(e[0], e[1], **propdict)
        else:
            raise ValueError('Topology is frozen, cannot be changed.')

    def topology_feedforward(self, n_in, n_middle, n_out):
        edges = npartite(n_in=n_in, n_middle=n_middle, n_out=n_out)
        self.topology_from_edges(edges)

    def learn(self, task, *, corrections=None, iterations=None,
              stop_on_success=True, return_ts=False, batch=True):
        """ Learning algorithm
        """
        if not self._topology_frozen:
            self.freeze_topology()

        if (corrections is None) and (iterations is None):
            # Default is run by iterations
            iterations = 10 * task.shape[0]
            corrections = np.inf
        elif (corrections is not None) and (iterations is None):
            iterations = np.inf
        elif (corrections is None) and (iterations is not None):
            corrections = np.inf

        # for book keeping
        history = dict(error=[], io=[], ts=[])

        def append2history(*, after_correction=False, end_of_batch=False):
            # book keeping
            if end_of_batch:
                history['error'].append(error)
            else:
                history['io'].append((valin, valout, valout_, after_correction))
                if return_ts:
                    history['ts'].append(ts)

        niter = 0
        nteach = 0
        tsklist = task.tolist()
        while True:
            error = task.shape[0]
            niter += 1
            if batch:
                # batch learning
                vio = self.process_inputs_from(task[:, 0])
                for vin, vout in vio[vio[:, 1] != task[:, 1]]:
                    nteach += 1
                    ein = self.input_encoding(vin)
                    # FIXME: decoding should be used here
                    eou = self.output_edge_idx[vout]
                    self.propagate_teaching(ein, eou)
                vio = self.process_inputs_from(task[:, 0])
                # FIXME: append ot history in batch_mode
                history['io'].append(vio)
            else:
                for valio in tsklist:
                    # process task input
                    valin, valout = valio
                    valout_, input_edgeid, output_edgeid, ts = self.process_input(valin)
                    append2history()
                    if valout_ != valout:
                        # Send feedback
                        nteach += 1
                        self.propagate_teaching(input_edgeid, output_edgeid)
                        valout_, _, _, ts = self.process_input(valin)
                        append2history(after_correction=True)
                vio = self.process_inputs_from(task[:, 0])

            error = np.sum(vio[:, 1] != task[:, 1])
            append2history(end_of_batch=True)
            if (error == 0) and stop_on_success:
                break

            if nteach >= corrections:
                if error != 0:
                    warnings.warn(
                       f'Reached maximum number of corrections ({corrections})!'
                       + f' Error: {error}',
                       UserWarning)
                break
            if niter >= iterations:
                warnings.warn(
                    f'Reached maximum number of iterations ({iterations})!'
                    + f' Error: {error}',
                    UserWarning)
                break

        return LearningResults(error=np.array(history['error']),
                               corrections=nteach,
                               iterations=niter,
                               io=np.array(history['io']),
                               extra=history['ts'])

    def propagate_input(self, input_edgeid):
        ts, mem_idx = self.integrate_network(self._insignal, self._dt,
                                             input_id=input_edgeid,
                                             kvl=self._kvl,
                                             kcl=self._kcl)
        # Update network to final state
        self.update_network(ts.current[-1, :], ts.voltage[-1, :], ts.resistance[-1, :],
                            ts.memristance[-1, :])
        return ts

    def learning_mode(self, input_edgeid, output_edgeid):
        assert self._topology_frozen
        # change the series resistance parameter of all memristors adjacent to
        # input and outputs not involved
        input_out = self.edges_array[self.input_out_edges_idx]
        opened_msk = input_out[:, 0] != self.edges_array[input_edgeid, 1]
        opened = tuple((*e, self.open_mem) for e in input_out[opened_msk])
        output_in = self.edges_array[self.output_in_edges_idx]
        opened_msk = output_in[:, 1] != self.edges_array[output_edgeid, 0]
        opened += tuple((*e, self.open_mem) for e in output_in[opened_msk])

        backup = []
        keys = tuple(self.open_mem.keys())
        for e in opened:
            propdict = {k: self[e[0]][e[1]][e[2]][k]
                        for k in keys}
            backup.append((*e[:3], propdict))

        self.update(edges=opened)
        return backup

    def propagate_teaching(self, input_edgeid, output_edgeid):

        # set the network in punishement mode
        backup = self.learning_mode(input_edgeid, output_edgeid)

        # propagate teaching signal
        ts, mem_idx = self.integrate_network(self._tchsignal, self._dt,
                                             input_id=input_edgeid)
        # set the network back to normal mode
        self.update(edges=backup)

        # Update network to final state
        self.update_network(ts.current[-1, :], ts.voltage[-1, :],
                            ts.resistance[-1, :],
                            ts.memristance[-1, :])
        return ts

    def output_decoding(self, currents):
        assert self._topology_frozen
        rms = np.sqrt(np.mean(currents[:, self.output_edge_idx]**2, axis=0))
        wta = np.argmax(rms)
        edgeid_out = self.output_edge_idx[wta]
        return wta, edgeid_out

    def input_encoding(self, integer):
        assert self._topology_frozen
        return self.input_edge_idx[integer]

    def process_inputs_from(self, values, *, reinforce=0.0):
        io = np.zeros((len(values), 2), dtype=int)
        for n, vin in enumerate(values):
            io[n, 1], _, _, _ = self.process_input(vin, reinforce=reinforce)
            io[n, 0] = vin
        return io

    def process_input(self, value, *, reinforce=0.0):
        edge_in = self.input_encoding(value)
        if reinforce > 0.0:
            # add mean value to data signal
            self._insignal += reinforce
        ts = self.propagate_input(edge_in)
        if reinforce > 0.0:
            # remove mean value to data signal
            self._insignal -= reinforce
        valout, edge_out = self.output_decoding(ts.current)
        return valout, edge_in, edge_out, ts


def data_signal(t, amp):
    return amp * np.sin(2 * np.pi * 5 * t)


def teaching_signal(t, amp):
    return amp * np.ones_like(t)


def func2str(fun):
    return '.'.join([fun.__module__, fun.__name__])
