"""
  General tools
"""
# Copyright (C) 2020 Juan Pablo Carbajal
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

# Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>

import numpy as np


def function_to_io(func, *, n_in, n_out=None, in_start=0, out_start=0):
    """ Map a function to input-output table

        Parameters
        ----------
        func: callable
            Function mapping the [0,1] interval to itself.
            The function should accept a :class:`numpy.ndarray` as input
            and return a :class:`numpy.ndarray`
        n_in: integer
            Number of input units
        n_out: integer (optional)
            Number of output units. Default is n_in.
        in_start,out_start: integer (optional)
            The starting value for the numbering of inputs and outputs.
            Default is 0.

        Returns
        -------
        :class:`numpy.ndarray`
            Input-output table filled with integers indicating the
            active input-output pair.
    """
    if n_out is None:
        n_out = n_in

    y = func(np.linspace(0, 1, n_in))
    outputs = np.round(y * (n_out-1)).astype(int)
    inputs = np.arange(0, n_in)

    return np.stack((inputs + in_start, outputs + out_start), axis=1)
