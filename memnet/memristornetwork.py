"""
  Memristor network utilities

"""
# Copyright (C) 2020 Juan Pablo Carbajal
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

# Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>

from collections import namedtuple

import numpy as np

from .electricnetwork import (
    ElectricNetwork,
    solve_currentbased,
)
from .components import (
    MemristorParameters,
    memristor_memristance,
    memristor_resistance
)


#: Storage for state timeseries
StateTimeSeries = namedtuple('StateTimeSeries', ['current', 'voltage',
                                                 'resistance', 'memristance'])


class MemristorNetwork(ElectricNetwork):
    """
    Electric network with memristive components
    """

    def __init(self):
        super().__init__()

    @property
    def memristor_param_array(self):
        dlist = [e[3] for e in self.edges(data=True, keys=True)
                  if 'Memristor' in e[3]]
        arrdict = {k: np.array([dic[k] for dic in dlist])
                   for k in MemristorParameters._fields}
        return MemristorParameters(**arrdict)

    @property
    def memristor_edges(self):
        return [e[:-1] for e in self.edges(data='Memristor', keys=True)
                if e[-1] is not None]

    # FIXME: this should look like a property when called without arguments
    # array = memristance_array
    # array = memristance_array(reduced=True)
    def memristance_array(self, *, reduced=False):
        marray = self.attr_array('memristance', missing=np.nan)
        if reduced:
            # TODO: do this with a single loop over edges
            idx = np.nonzero(~np.isnan(marray))[0]
            return marray[idx], idx
        return marray

    def set_memristance(self, mem):
        self.update_memristors(mem)

    def update_memristors(self, mem=None, res=None):
        medge = self.memristor_edges
        nmem = len(medge)
        if mem is not None:
            if res is not None:
                if (len(mem) != nmem) or (len(res) != nmem):
                    raise ValueError('Number of memristances/resistances' +
                                     f'{len(mem)}/{len(res)} must equal ' +
                                     f'number of memristors {nmem}')
            else:
                if len(mem) != nmem:
                    raise ValueError('Number of memristances' +
                                     f'{len(mem)} must equal ' +
                                     f'number of memristors {nmem}')
                res = memristor_resistance(mem, self.memristor_param_array)

        else:
            mem, _ = self.memristance_array(reduced=True)
            res = memristor_resistance(mem, self.memristor_param_array)

        datadict = [dict(memristance=w, resistance=r) for w, r in zip(mem, res)]
        self.update((*e[:3], d) for e, d in zip(medge, datadict))

    def integrate_network(self, vcc, dt, *, input_id=None, kvl=None, kcl=None):
        if input_id is None:
            input_id = self.voltage_source_index
        else:
            input_id = np.atleast_1d(input_id)

        steps, sources = vcc.shape
        if sources != len(input_id):
            raise ValueError(f'Number of voltages time series {sources}' +
                             f' must match voltage sources {len(input_id)}')
        edges = list(self.edges)
        nedges = len(edges)
        vs = np.zeros(nedges)
        vs[input_id] = vcc[0, :]

        V = np.zeros((steps, nedges))
        I = np.zeros((steps, nedges))

        res = self.resistance_array
        R = np.repeat(np.atleast_2d(res), steps, axis=0)

        kws = dict(KVL=self.kvl if kvl is None else kvl,
                   KCL=self.kcl if kcl is None else kcl)
        # Prime the network
        I[0, :], V[0, :], _ = solve_currentbased(resistance=R[0,:],
                                                 source=vs, **kws)
        mem, id_mem = self.memristance_array(reduced=True)
        w = np.zeros((steps, len(id_mem)))
        w[0, :] = mem
        memparams = self.memristor_param_array
        w[0, :] = memristor_memristance(w[0, :], I[0, id_mem], memparams,
                                        dt=dt)
        # Integrate the time varying voltages
        for n, v in enumerate(vcc[1:, :], start=1):
            w[n, :] = memristor_memristance(w[n - 1, :], I[n - 1, id_mem],
                                            memparams, dt=dt)
            R[n, id_mem] = memristor_resistance(w[n, :], memparams)
            vs[input_id] = v
            I[n, :], V[n, :], _ = solve_currentbased(resistance=R[n, :],
                                                     source=vs, **kws)
        return StateTimeSeries(current=I, voltage=V, resistance=R,
                               memristance=w), id_mem

    def update_network(self, I, V, R, w):
        edges = self.edges
        ne = len(edges)
        if (len(I) != ne) or (len(V) != ne) or (len(R) != ne):
            raise ValueError('Wrong sizes')

        datadict = [dict(voltage=v, current=i, resistance=r)
                    for v, i, r in zip(V, I, R)]
        self.update((*e[:3], d) for e, d in zip(edges, datadict))
        self.update_memristors(w)
