"""
  Component utilities

"""
# Copyright (C) 2020 Juan Pablo Carbajal
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

# Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>

from collections import namedtuple

import numpy as np


class Component:

    def __init__(self, mytype=None):
        self.current = 0.0
        self.voltage = 0.0
        self._type = type(self).__name__ if mytype is None else mytype

    def asdict(self):
        retval = self.__dict__.copy()
        # TODO: could use set_attr in __init__
        retval[retval.pop('_type')] = True
        return retval


class Resistor(Component):

    def __init__(self, value=0.0):
        super().__init__(self.__class__.__name__)
        self.resistance = value


class VoltageSource(Component):

    def __init__(self, value=0.0):
        super().__init__(type(self).__name__)
        self.voltage = value


class Memristor(Component):

    def __init__(self, *, memristance=0.0, low_R=1, high_R=100, gain=100,
                 polarity=1, series_R=0.0):
        super().__init__(type(self).__name__)
        self.low_R = _ctrl_nonngetative(low_R)
        self.high_R = _ctrl_nonngetative(high_R)
        self.series_R = _ctrl_nonngetative(series_R)
        self.gain = _ctrl_nonngetative(gain)
        self.memristance = _ctrl_inunit(memristance)
        self.polarity = np.sign(polarity)
        self.resistance = memristor_resistance(self.memristance,
                                               low_R=self.low_R,
                                               high_R=self.high_R,
                                               series_R=self.series_R)


def memristor_resistance(w, parameters=None, *, low_R=None, high_R=None,
                         series_R=0.0):
    if parameters is not None:
        low_R = parameters.low_R
        high_R = parameters.high_R
        series_R = parameters.series_R
    res_eq = high_R + (low_R - high_R) * w + series_R
    return res_eq


def memristor_memristance(w0, I, parameters=None, *, dt, polarity=None,
                          gain=None):
    if parameters is not None:
        polarity = parameters.polarity
        gain = parameters.gain
    w = w0 + polarity * gain * I * dt
    w[w > 1.0] = 1.0
    w[w < 0.0] = 0.0
    return w


def _ctrl_nonngetative(p):
    if p >= 0:
        return p
    raise ValueError('parameter must be positive')


def _ctrl_inunit(p):
    if (p <= 1) and (p >= 0):
        return p
    raise ValueError('parameter must be between 0 and 1 (inclusive)')


#: Storage for memristor parameters
# TODO: should this be a class attribute?
MemristorParameters = namedtuple('MemristorParameters',
                                 [k for k in Memristor().asdict()
                                  if (k not in Resistor().asdict()) and
                                  (k not in ('memristance', 'Memristor'))])
