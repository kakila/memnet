"""
  Electric network utilities

"""
# Copyright (C) 2020 Juan Pablo Carbajal
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

# Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>

from collections import Iterable
from copy import deepcopy

import networkx as nx
import numpy as np
import sympy as sm

from .kirchhof import (
    kvl,
    kcl
)

from .components import Component


class ElectricNetwork(nx.MultiDiGraph):
    """
        Electric network class.

        An electric network is a MultiDiGraph.

        Components of the network are placed as data in edges.
    """

    def __init__(self):
        super().__init__()

    def add_component(self, u_for_edge, v_for_edge, key=None, *,
                      component=None, **kwargs):
        if component is None:
            component = Component()
        self.add_edge(u_for_edge, v_for_edge, key,
                      **component.asdict(), **kwargs)

    def attr_mask(self, attr):
        return np.array([e[-1] is not None
                        for e in self.edges(data=attr, keys=True)])

    def attr_array(self, attr, *, missing=None, reduced=False):
        if reduced:
            return np.array([e[-1] for e in self.edges(data=attr, keys=True,
                                                       default=missing)
                             if not e[-1] is missing])
        else:
            return np.array([e[-1] for e in self.edges(data=attr, keys=True,
                                                   default=missing)])

    def _set_edge_attr(self, edges, values, attr):
        """ Internal function """
        if not isinstance(values, Iterable):
            values = (values,) * len(edges)

        if len(values) != len(edges):
            raise ValueError('Edges and values have different length '
                             + 'and values is not scalar')

        self.update((*e, {attr: v}) for e, v in zip(edges, values))

    @property
    def voltage_array(self):
        return self.attr_array('voltage')

    @property
    def current_array(self):
        return self.attr_array('current')

    @property
    def iv_array(self):
        # TODO: attr_ method that retrieves multiple attrs in a single loop
        current = self.attr_array('current')
        voltage = self.attr_array('voltage')
        return current, voltage

    @property
    def resistance_array(self):
        return self.attr_array('resistance', missing=0.0)

    @property
    def voltage_source_index(self):
        return np.nonzero(self.attr_mask('VoltageSource'))[0]

    @property
    def kvl(self):
        # Conserve edge order
        g = nx.MultiDiGraph()
        g.add_nodes_from(self.nodes)
        g.add_edges_from(self.edges)
        return kvl(g)

    @property
    def kcl(self):
        return kcl(self)

    def update_iv(self, current, voltage):
        if len(voltage) != len(current):
            raise ValueError('Current and voltages must have the same length')
        nedges = len(self.edges)
        if len(current) != nedges:
            raise ValueError('Wrong number of currents')
        if len(voltage) != nedges:
            raise ValueError('Wrong number of voltages')

        datadict = [dict(current=i, voltage=v) for i, v in zip(current, voltage)]
        self.update((*e, d) for e, d in zip(self.edges, datadict))

    def update_resistance(self, res):
        nedges = len(self.edges)
        if len(res) != nedges:
            raise ValueError('Wrong number of resistances')

        datadict = [dict(resistance=r) for r in res]
        self.update((*e, d) for e, d in zip(self.edges, datadict))

    def solve_iv(self):
        Vcc = self.voltage_array[self.voltage_source_index]
        return solve_currentbased(resistance=self.resistance_array,
                           source=Vcc,
                           KVL=self.kvl,
                           KCL=self.kcl)

    def deepcopy(self):
        return deepcopy(self)


def solve_currentbased(*, resistance, source, KVL, KCL, rhs=None):
    """ Compute voltages and current in the electric network, solving for currents
    """
    nedges = KCL.shape[1]

    idx_source = source.nonzero()[0]
    if rhs is None:
        # put voltage sources on rhs
        rhs = np.zeros(nedges)
        for n in idx_source:
            idx = np.nonzero(KVL[:, n])[0]
            values = KVL[idx, n] * source[n]
            try:
                rhs[idx] = rhs[idx] + values
            except TypeError:
                # handle non-numeric types, e.g. sympy symbols
                rhs = rhs.astype(object)
                rhs[idx] = rhs[idx] + values

    # Remove edges that are in RHS
    KVLo = KVL.copy()
    KVLo[:, idx_source] = 0

    #R = np.diag(resistance)
    #M = np.vstack((KVLo @ R, KCL))
    M = np.zeros((KVLo.shape[0] + KCL.shape[0], KCL.shape[1]))
    try:
        M[:KVLo.shape[0], :] = KVLo * resistance
        M[KVLo.shape[0]:, :] = KCL
        I = np.linalg.solve(M, rhs)
    except TypeError:
        M = M.astype(object)
        M[:KVLo.shape[0], :] = KVLo * resistance
        M[KVLo.shape[0]:, :] = KCL
        # Solve using sympy
        system = sm.Matrix(np.hstack((M,
                           np.atleast_2d(rhs).T)).tolist())
        symbname = sm.symbols(r'I:{n}'.format(n=nedges))
        I = sm.solve_linear_system(system, *symbname)
        I = np.array([I[x].simplify().together() for x in symbname])

    #V = R @ I
    V = resistance * I

    # put sources as negative voltage drops
    V[idx_source] = -rhs[idx_source]

    extra = {
        'rhs': rhs,
        'Matrix': M
            }
    return I, V, extra
