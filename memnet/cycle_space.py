# Copyright (C) 2020 Juan Pablo Carbajal
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

# Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>

import networkx as nx
import numpy as np


def chords(G):
    """ Return a new graph that contains the edges that are the chords of G.

        The chords are all the edges that are not in a spanning three of G.

        Parameters
        ----------
        G : graph
         A NetworkX graph.

        Returns
        -------
        C :
            A new graph with the chords of G.
        T :
            The spanning tree from which C was calculated.
        """
    if G.is_directed():
        if G.is_multigraph():
            T = nx.minimum_spanning_tree(nx.MultiGraph(G))
        else:
            T = nx.minimum_spanning_tree(nx.Graph(G))
    else:
        T = nx.minimum_spanning_tree(G)

    C = G.copy()
    edges = T.edges()

    for e in edges:
        try:
            C.remove_edge(*e)
        except:
            C.remove_edge(*e[::-1])

    # Recreate T to get the same type as G
    T = G.copy()
    if G.is_multigraph():
        edges = C.edges(keys=True)
    else:
        edges = C.edges()

    for e in edges:
        T.remove_edge(*e)

    return C, T


def cycle_space(C, T):
    """ Return a list of cycle graphs representing the fundamental cycles of the
        spanning tree T extended by the chords edges of the graph C

        Parameters
        ----------
        T: graph
            A tree graph.
        C: (multidi)graph
            Graph representing chords for the tree T.

        Returns
        -------
        Z:
            list of cycles

    """
    Z = list()
    edges = C.edges()

    for idx,e in enumerate(edges):
        if T.has_edge(*e) or T.has_edge(*e[::-1]):
            Z.append(list(e))
        else:
            T.add_edge(*e)
            Z.append(nx.cycle_basis(nx.Graph(T))[0])
            T.remove_edge(*e)

    return Z


def cycle_space_matrix(G, C, T):
    """ Return a the matrix describing the fundamental cycles in G.

        If G is not oriented and arbitrary orientation is taken.

        Parameters
        ----------
        T: graph
            A tree graph of G
        C: graph
            Graph representing chords for the tree T.

        Returns
        -------
        M:
            Matrix of the fundamental cycles
    """

    if G.is_multigraph():
        Cedges = list(C.edges(keys=True))
        Gedges = list(G.edges(keys=True))
        Tedges = list(T.edges(keys=True))
    else:
        Cedges = list(C.edges())
        Gedges = list(G.edges())
        Tedges = list(T.edges())

    nrow = len(Gedges)
    ncol = len(Cedges)
    M    = np.zeros([nrow,ncol],dtype=np.int)

    for col, e in enumerate(Cedges):
        row = Gedges.index(e)
        M[row,col]  = 1

        Tedges_2 = list(T.edges())
        try:
            einT  = Tedges_2.index(e[:2])
            # The edge is in the tree with the same orientation, hence invert it
            row2        = Gedges.index(Tedges[einT])
            M[row2,col] = -1
        except ValueError:
            try:
                ieinT = Tedges_2.index(e[:2][::-1])
                # The edge is in the tree with the opposite orientation, hence leave it
                row2        = Gedges.index(Tedges[ieinT])
                M[row2,col] = 1
            except ValueError:
                tmp = Tedges_2[:]
                tmp.append(e[:2])
                cyc = nx.cycle_basis(nx.Graph(tmp))[0]
                cyc_e = []

                for idx, node in enumerate(cyc):
                    if  idx < len(cyc)-1:
                        cyc_e.append((node, cyc[idx+1]))
                    else:
                        cyc_e.append((node, cyc[0]))

                if e[:2][::-1] in cyc_e:
                    # The chord is in the cycle with the opposite orientation
                    # invert all edges of the cycle
                    cyc_e = [i[::-1] for i in cyc_e]
                elif e[:2] not in cyc_e:
                      raise NameError('Something went wrong! ' + 
                                      f'The edge {e} is not in cycle {cyc}')

                cyc_e.remove(e[:2])
                for ce in cyc_e:
                    try:
                        einT  = Tedges_2.index(ce)
                        # The edge is in the tree with the same orientation
                        row2        = Gedges.index(Tedges[einT])
                        M[row2,col] = 1
                    except ValueError:
                        ieinT = Tedges_2.index(ce[::-1])
                        # The edge is in the tree with the opposite orientation
                        row2        = Gedges.index(Tedges[ieinT])
                        M[row2,col] = -1

    return M


def rref(matrix):
    """ Row echelon form """

    numRows,numCols = matrix.shape
    zero = np.finfo(np.float).eps

    was_int = []
    if matrix.dtype.name[0:3] == 'int':
        was_int = matrix.dtype.name
        matrix = matrix.astype(np.float)

    i,j = 0,0
    while True:
        if i >= numRows or j >= numCols:
            break

        if np.abs(matrix[i,j]) <= zero:
            nonzeroRow = i
            while nonzeroRow < numRows and np.abs(matrix[nonzeroRow,j]) <= zero:
                nonzeroRow += 1

            if nonzeroRow == numRows:
                j += 1
                continue

            matrix[(i,nonzeroRow),:] = matrix[(nonzeroRow,i),:]

        matrix[i,:] = matrix[i,:]/matrix[i,j]

        for otherRow in xrange(0, numRows):
             if otherRow == i:
                 continue
             if np.abs(matrix[otherRow,j]) > zero:
                 matrix[otherRow,:] = matrix[otherRow,:] - matrix[otherRow,j]*matrix[i,:]

        i += 1; j+= 1

    if was_int:
        return matrix.astype(was_int)
    else:
        return matrix