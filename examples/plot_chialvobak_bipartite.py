"""
Bipartite Chialvo-Bak network
=============================

"""
# %%
# Copyright (C) 2020 Juan Pablo Carbajal
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

# Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>

from pathlib import Path
from textwrap import dedent

import numpy as np
from ttictoc import tic, toc

import memnet.learningmachines as lm

# %%
# Helper functions
# ----------------
# I/O table formatter
def iotable_str(tsk):
    txt = 'input\toutput\n'
    for row in tsk:
        txt += '\t'.join(map(str, row)) + '\n'
    return txt

# %%
# Create learning machine
# -----------------------
#
machine = lm.ChialvoBak()
n_in = 2
n_out = 2
machine.topology_feedforward(n_in, 0, n_out)
machine.freeze_topology()

# number of memristors
nmem = len(machine.memristor_edges)
w = np.random.rand(nmem)
machine.set_memristance(w)

machine.teach_amplitude = -0.1

print(machine)

# %%
# Define the task table
# ---------------------
#
task = np.cumsum(np.ones((n_in, 2)), axis=0, dtype=int) - 1

print('Task:')
print(iotable_str(task))

# %%
# Attempt learning
# ----------------
#
tic()
res = machine.learn(task)
print(f'Error: {res.error[-1]}\n'
      + f'Iterations: {res.iterations}\n'
      + f'Corrections: {res.corrections}')
print(f'Elapsed time {toc()} s')

# check output
final_io = machine.process_inputs_from(task[:, 0])
print('Final I/O:')
print(iotable_str(final_io))

# %%
# Make more attempts
# ------------------
#
np.random.seed(123454321)
ntry = 200

best_error = np.inf
learned = 0
lucky = 0
bad_init = []
tic()
for n in range(ntry):
    w = np.random.rand(nmem)
    machine.set_memristance(w)
    # If solves without learning, ignore
    if np.allclose(machine.process_inputs_from(task[:, 0])[:, 1], task[:, 1]):
        lucky += 1
        continue
    # Prime the network
    for _ in range(3):
        machine.process_inputs_from(task[:, 0], reinforce=True)
    res = machine.learn(task)
    if res.error[-1] < best_error:
        best_error = res.error[-1]
    if res.error[-1] < 1e-3:
        learned += 1
    else:
        bad_init.append((res.error[-1], w))

print(f'Elapsed time {toc()} s')
print(f'Best error from {ntry} random networks: {best_error}')
print(f'Successful attempts:  {100.0 * learned / ntry:.1f}% ({learned}/{ntry})')
print(f'Lucky initializations:  {100.0 * lucky / ntry:.1f}% ({lucky}/{ntry})')
print(f'Overall success:  {100.0 * (learned+lucky) / ntry:.1f}% '
      + f'({learned+lucky}/{ntry})')
