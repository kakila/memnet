"""
Build Chialvo-Bak network from YAML
===================================

"""
# %%
# Copyright (C) 2020 Juan Pablo Carbajal
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

# Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>

from pathlib import Path
import memnet.learningmachines as lm

# %%
# Create learning machine
# -----------------------
#
try:
    filename = Path(__file__).parent / 'cbm.yml'
except NameError:
    # Probably executed by sphinx
    filename = Path('../examples').resolve() / 'cbm.yml'
machine = lm.ChialvoBak.from_yaml(filename)
print(machine)
