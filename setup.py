#!/usr/bin/env python3

""" setup script for memnet module """

# Copyright (C) 2020 JuanPi Carbajal
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

# Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>

from setuptools import (
    setup,
    find_packages
)

from memnet import (
    __version__,
    __author__,
    __email__
)

with open('README.rst', encoding='utf-8') as f:
    long_description = f.read()

setup(
    name="memnet",
    packages=find_packages(exclude=["tests*"]),
    version=__version__,
    install_requires=["scipy",
                      "numpy",
                      "matplotlib",
                      "networkx",
                      "sympy",
                      "pyyaml",
                      "ttictoc"],
    author=__author__,
    author_email=__email__,
    url="https://gitlab.com/kkila/memnet",
    classifiers=["Development Status :: 3 - Alpha",
                 "Environment :: Other Environment",
                 "Intended Audience :: Science/Research",
                 "License :: OSI Approved :: GNU General Public License v3 or later (GPLv3+)",
                 "Operating System :: OS Independent",
                 "Programming Language :: Python :: 3",
                 "Topic :: Scientific/Engineering",
                 "Topic :: Scientific/Engineering :: Information Analysis",
                 ],
    description="Memristor networks",
    long_description=long_description,
    long_description_content_type='text/x-rst',
    scripts=[
            'scripts/memnet_chialvobak1x2x2.py',
            'scripts/memnet_chialvobak.py'],
)
