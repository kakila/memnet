Welcome to MemNet's documentation!
===================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   overview

   scripts

   API

   auto_examples/index.rst

.. toctree::
   :maxdepth: 1
   :caption: Examples:


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
