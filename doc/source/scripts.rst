
=====================
Scripts documentation
=====================

.. contents:: :local:


Some MemNets
************

Chialvo-Bak 1x2x2
-----------------
.. automodule:: scripts.memnet_chialvobak1x2x2
   :members:
   :undoc-members:

Chialvo-Bak
------------
.. automodule:: scripts.memnet_chialvobak
   :members:
   :undoc-members:

